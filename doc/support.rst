.. _support:

Support and Development
-----------------------

To get help using the **signac** package, either send an email to `signac-support@umich.edu <mailto:signac-support@umich.edu>`_ or join the `signac gitter chatroom <https://gitter.im/signac/Lobby>`_.

The **signac** package is hosted on `Bitbucket <https://bitbucket.org/glotzer/signac>`_ and licensed under the open-source BSD 3-Clause license.
Please use the `repository's issue tracker <https://bitbucket.org/glotzer/signac/issues?status=new&status=open>`_ to report bugs or request new features.

